import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashCheckin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RTLMain();
  }
}

class RTLMain extends StatefulWidget {
  @override
  _RTLMainState createState() => new _RTLMainState();
}

class _RTLMainState extends State<RTLMain> {
  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  bool loadding = true;

  void navigationPage() {
    Navigator.pushNamed(
      context,
      '/login',
    );
  }

  @override
  void initState() {
    super.initState();

    startTime();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);

    return new Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Center(
          child: Container(
              child: Image.asset(
            'images/2sow.png',
            width: 125,
            height: 125,
          )),
        ),
      ),
    );
  }
}
