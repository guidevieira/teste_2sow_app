import 'package:flutter/material.dart';
// import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class MyTextFormField extends StatelessWidget {
  final String hintText;
  final String labelText;
  final String initValue;
  final Function validator;
  final Function onSaved;
  final Function onChanged;
  final IconButton suffixIcon;
  final bool isPassword;
  final teste;
  final isEmail;

  final controller;

  MyTextFormField({
    this.hintText,
    this.validator,
    this.onSaved,
    this.onChanged,
    this.isPassword = false,
    this.isEmail = false,
    this.labelText,
    this.suffixIcon,
    this.initValue,
    this.controller,
    this.teste,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16),
      child: TextFormField(
        inputFormatters: teste != null ? [teste] : [],
        controller: controller,
        initialValue: initValue,
        decoration: InputDecoration(
          hintText: hintText,
          contentPadding: EdgeInsets.all(20.0),
          border: OutlineInputBorder(),
          // labelText: labelText,
          labelStyle: TextStyle(fontSize: 20.0, color: Colors.grey),
          labelText: labelText,
          filled: true,
          suffixIcon: this.suffixIcon,
          fillColor: Color(0xFFEEEEF3),
        ),
        obscureText: isPassword ? true : false,
        validator: validator,
        onSaved: onSaved,
        onChanged: onChanged,
        keyboardType: isEmail ? TextInputType.emailAddress : TextInputType.text,
      ),
    );
  }
}
