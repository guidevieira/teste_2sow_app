import 'package:hasura_connect/hasura_connect.dart';
import 'package:twosow/app/app_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:twosow/app/app_widget.dart';
import 'package:twosow/app/modules/home/home_module.dart';

import 'modules/home/repositories/home_repository.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController(i.get<HomeRepository>())),
        Bind((i) => HomeRepository(i.get<HasuraConnect>())),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
