import 'package:mobx/mobx.dart';

import 'modules/home/repositories/home_repository.dart';

part 'app_controller.g.dart';

class AppController = _AppControllerBase with _$AppController;

abstract class _AppControllerBase with Store {
  _AppControllerBase(this._repository);
  final HomeRepository _repository;

  @observable
  int value = 0;
}
