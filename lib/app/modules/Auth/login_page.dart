import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:twosow/app/modules/Auth/login_form.dart';
import 'package:twosow/app/utils/commons/auth_header.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    // final themeColor = Provider.of<ThemeNotifier>(context);
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
          statusBarColor: Color(0xffE37740),
          systemNavigationBarIconBrightness: Brightness.dark,
          statusBarBrightness: Brightness.dark,
          statusBarIconBrightness: Brightness.dark),
    );
    return Container(
      color: Color(0xffE37740),
      child: SafeArea(
        bottom: false,
        child: WillPopScope(
            onWillPop: () async => false,
            child: Scaffold(
              resizeToAvoidBottomInset: true,
              backgroundColor: Colors.white,
              body: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    AuthHeader(
                        headerTitle: "Entrar",
                        headerBigTitle: "Entrar",
                        isLoginHeader: true),
                    SizedBox(
                      height: 36,
                    ),
                    LoginForm(),
                    SizedBox(
                      height: 8,
                    ),
                    routeRegisterWidget(context)
                  ],
                ),
              ),
            )),
      ),
    );
  }

  routeRegisterWidget(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 42, left: 42, bottom: 12),
      child: Row(
        children: <Widget>[
          Text(
            "Nao possui conta?",
            style: GoogleFonts.poppins(
              fontSize: 14,
              color: Colors.black,
              fontWeight: FontWeight.w200,
            ),
          ),
          FlatButton(
            child: Text(
              "Criar Conta",
              style: GoogleFonts.poppins(
                fontSize: 14,
                color: Color(0xffE37740),
                fontWeight: FontWeight.w600,
              ),
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }
}
