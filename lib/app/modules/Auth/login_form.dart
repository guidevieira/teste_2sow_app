import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:twosow/app/app_controller.dart';
import 'package:twosow/app/utils/commons/custom_textfield.dart';
import 'package:twosow/app/utils/screen.dart';

import 'package:validators/validators.dart' as validator;

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends ModularState<LoginForm, AppController> {
  final _formKey = GlobalKey<FormState>();
  bool passwordVisible = false;

  @override
  void initState() {
    super.initState();
  }

  bool loading = false;

  fetchPost() async {
    // final result = await controller.loginUser(model);
    // print(result);
    if (true == true) {
      Navigator.pushNamed(context, '/home');
      setState(() {
        loading = false;
      });
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // final themeColor = Provider.of<ThemeNotifier>(context);

    return Container(
      padding: EdgeInsets.only(top: 24, right: 42, left: 42),
      child: SafeArea(
          bottom: true,
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                MyTextFormField(
                  labelText: "Email",
                  hintText: 'Email',
                  isEmail: true,
                  validator: (String value) {
                    if (!validator.isEmail(value)) {
                      return 'Email inválido.';
                    }

                    return null;
                  },
                ),
                MyTextFormField(
                  labelText: "Senha",
                  hintText: 'Senha',
                  suffixIcon: IconButton(
                    icon: Icon(
                      passwordVisible ? Icons.visibility : Icons.visibility_off,
                      color: Color(0xffE37740),
                    ),
                    onPressed: () {
                      setState(() {
                        passwordVisible = !passwordVisible;
                      });
                    },
                  ),
                  isPassword: passwordVisible,
                  validator: (String value) {
                    if (value.length < 4) {
                      return 'Senha muito curta';
                    }

                    _formKey.currentState.save();

                    return null;
                  },
                  onSaved: (String value) {},
                ),
                loading != true
                    ? Container(
                        height: 42,
                        width: ScreenUtil.getWidth(context),
                        margin: EdgeInsets.only(top: 32, bottom: 12),
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(8.0),
                          ),
                          color: Color(0xffE37740),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              setState(() {
                                loading = true;
                              });
                              _formKey.currentState.save();

                              FutureBuilder(
                                  future: fetchPost(),
                                  builder: (context, snapshot) {
                                    print(snapshot);
                                  });
                            }
                          },
                          child: Text(
                            'Entrar',
                            style: GoogleFonts.poppins(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      )
                    : Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Container(
                          child: CircularProgressIndicator(),
                        ),
                      ),
              ],
            ),
          )),
    );
  }
}
