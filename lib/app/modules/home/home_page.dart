import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:twosow/app/SideBar/navDrawer.dart';
import 'home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:twosow/app/modules/home/home_module.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter/services.dart';

class HomePage extends StatefulWidget {
  final String title;
  const HomePage({Key key, this.title = "Home"}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends ModularState<HomePage, HomeController> {
  HomeController homeController = HomeModule.to.get();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
    ));
    init();
  }

  @override
  void dipose() {
    super.dispose();
  }

  String name = "";
  init() async {
    final SharedPreferences prefs = await _prefs;
    setState(() {
      name = prefs.getString('user_name');
    });
  }

  onDelete(String id) async {
    print(id);
    await controller.onDelete(id);
    print(id);
  }

  // bool oneclick = false;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          appBar: AppBar(
            title: Text("usuarios"),
            backgroundColor: Color(0xffE37740),
            automaticallyImplyLeading: false,
          ),
          endDrawer: NavDrawer(),
          backgroundColor: Color(0xffEEF3F6),
          body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: MediaQuery.of(context).size.height,
              color: Color(0xffEEF3F6),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // listview
                  Expanded(
                    child: Center(
                      child: Observer(
                        builder: (BuildContext context) {
                          if (homeController.user_details.value == null) {
                            return Center(child: CircularProgressIndicator());
                          }

                          if (homeController.user_details.hasError == null) {
                            return Center(
                              child: Text('erro'),
                            );
                          }

                          return ListView.builder(
                              itemCount:
                                  homeController.user_details.value.length,
                              scrollDirection: Axis.vertical,
                              itemBuilder: (BuildContext context, int i) {
                                return Card(
                                  child: ListTile(
                                    title: Text(homeController
                                        .user_details.value[i].name),
                                    trailing: Wrap(
                                      spacing: 12, // space between two icons
                                      children: <Widget>[
                                        IconButton(
                                          icon: Icon(
                                            MaterialIcons.edit,
                                            color: Colors.yellow,
                                          ),
                                          highlightColor: Colors.pink,
                                          onPressed: () => {
                                            Navigator.pushReplacementNamed(
                                              context,
                                              '/editUser',
                                              arguments: homeController
                                                  .user_details.value[i],
                                            ),
                                          },
                                        ),
                                        IconButton(
                                          icon: new Icon(
                                            MaterialIcons.delete_forever,
                                            color: Colors.red,
                                          ),
                                          onPressed: () => onDelete(
                                              homeController
                                                  .user_details.value[i].id),
                                        ), // icon-1
                                      ],
                                    ),
                                  ),
                                );
                              });
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.pushNamed(context, '/createUser');
            },
            child: Icon(Icons.add),
            backgroundColor: Color(0xffE37740),
          ),
        ));
  }
}
