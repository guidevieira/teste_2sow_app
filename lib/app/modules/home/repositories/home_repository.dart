import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:hasura_connect/hasura_connect.dart';
import 'package:twosow/app/modules/home/models/user_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeRepository extends Disposable {
  final HasuraConnect _hasuraConnect;
  HomeRepository(this._hasuraConnect);

  bool logado = false;

  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Stream<List<UserModel>> getUser() {
    var subscription = '''
   subscription MySubscription {
  users {
    name
    cpf
    email
    cep
    endereco
    numero
    bairro
    cidade
    id
  }
}



    ''';

    var snapshot = _hasuraConnect.subscription(subscription);
    return snapshot
        .map((data) => UserModel.fromJsonList(data['data']['users']));
  }

  Future onDelete(String uuid) {
    var subscription = '''
   mutation {
    delete_users(where: { id: { _eq: "$uuid" } }) {
      returning {
        name
        id
        email
      }
    }
  }


    ''';
    var snapshot = _hasuraConnect.mutation(subscription);
    return snapshot;
  }

  Future createUser(dynamic user) {
    int number = int.parse(user['numero']);
    var subscription = '''
  mutation{
    insert_users(
      objects: {
        cpf: "${user['cpf']}"
        email: "${user['email']}"
        name: "${user['name']}"
        cep: "${user['cep']}"
        endereco: "${user['endereco']}"
        numero: $number
        bairro: "${user['bairro']}"
        cidade: "${user['cidade']}"
      }
    ) {
      returning {
        name
        email
        cpf
      }
    }
  }

    ''';
    var snapshot = _hasuraConnect.mutation(subscription);
    return snapshot;
  }

  Future editUser(dynamic user) {
    int number = int.parse(user['numero']);
    var subscription = '''
  mutation {
    update_users(
      where: { id: { _eq: "${user['id']}" } }
      _set: {
         cpf: "${user['cpf']}"
        email: "${user['email']}"
        name: "${user['name']}"
        cep: "${user['cep']}"
        endereco: "${user['endereco']}"
        numero: $number
        bairro: "${user['bairro']}"
        cidade: "${user['cidade']}"
      }
    ) {
      returning {
        id
      }
    }
  }

    ''';
    var snapshot = _hasuraConnect.mutation(subscription);
    return snapshot;
  }

  String rua;
  Future getCep(cep) async {
    try {
      Response response =
          await Dio().get("https://viacep.com.br/ws/$cep/json/");

      rua = response.data['logradouro'];
      return [
        rua,
        response.data['bairro'],
        response.data['localidade'],
        response.data['uf'],
      ];
    } catch (e) {
      print(e);
    }
  }

  //dispose will be called automatically
  @override
  void dispose() {}
}
