class UserModel {
  String name;
  String cpf;
  String email;
  String cep;
  String endereco;
  int numero;
  String bairro;
  String cidade;
  String id;

  UserModel(
      {this.name,
      this.cpf,
      this.email,
      this.cep,
      this.endereco,
      this.numero,
      this.bairro,
      this.cidade,
      this.id});

  UserModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    cpf = json['cpf'];
    email = json['email'];
    cep = json['cep'];
    endereco = json['endereco'];
    numero = json['numero'];
    bairro = json['bairro'];
    cidade = json['cidade'];
    id = json['id'];
  }

  static List<UserModel> fromJsonList(List list) {
    if (list == null) return null;
    return list.map<UserModel>((item) => UserModel.fromJson(item)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['cpf'] = this.cpf;
    data['email'] = this.email;
    data['cep'] = this.cep;
    data['endereco'] = this.endereco;
    data['numero'] = this.numero;
    data['bairro'] = this.bairro;
    data['cidade'] = this.cidade;
    data['id'] = this.id;
    return data;
  }
}
