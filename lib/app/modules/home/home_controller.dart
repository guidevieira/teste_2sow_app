import 'package:mobx/mobx.dart';
import 'package:twosow/app/modules/home/models/user_model.dart';
import 'package:twosow/app/modules/home/repositories/home_repository.dart';

part 'home_controller.g.dart';

class HomeController = _HomeControllerBase with _$HomeController;

abstract class _HomeControllerBase with Store {
  final HomeRepository _repository;
  _HomeControllerBase(this._repository) {
    user_details = ObservableStream(_repository.getUser());
  }

  @action
  Future onDelete(String uuid) async {
    await _repository.onDelete(uuid);
    return;
  }

  @action
  Future getCep(String cep) async {
    var res = await _repository.getCep(cep);
    return res;
  }

  @action
  Future createUser(dynamic user) async {
    var res = await _repository.createUser(user);
    return res;
  }

  @action
  Future editUser(dynamic user) async {
    var res = await _repository.editUser(user);
    return res;
  }

  @observable
  ObservableStream<List<UserModel>> user_details;
}
