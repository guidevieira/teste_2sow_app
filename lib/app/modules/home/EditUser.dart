import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:twosow/app/modules/home/home_controller.dart';
import 'package:twosow/app/modules/home/home_module.dart';
import 'package:twosow/app/utils/commons/custom_textfield.dart';

class EditUser extends StatefulWidget {
  final users;

  const EditUser({
    Key key,
    this.users,
  }) : super(key: key);

  @override
  @override
  _editUser createState() => _editUser();
}

class _editUser extends ModularState<EditUser, HomeController> {
  HomeController homeController = HomeModule.to.get();
  GlobalKey<FormState> _formKey2 = GlobalKey<FormState>();

  var maskFormatter = new MaskTextInputFormatter(
      mask: '###.###.###-##', filter: {"#": RegExp(r'[0-9]')});

  String name;
  String email;
  String cpf;
  String numero;
  String rua;
  String bairro;
  String cidade;
  String cep;
  bool loadding = false;

  init() async {
    print(widget.users);
  }

  @override
  void initState() {
    super.initState();
    init();
  }

  editUser() async {
    setState(() {
      loadding = true;
    });
    dynamic send = {
      "name": name,
      "email": email,
      "cpf": cpf,
      "cep": cep,
      "numero": numero,
      "cidade": cidade,
      "bairro": bairro,
      "endereco": rua,
      "id": widget.users.id,
    };
    await homeController.editUser(send);
    setState(() {
      loadding = false;
    });
    Navigator.pushNamed(context, '/home');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          backgroundColor: Color(0xffE37740),
          leading: new IconButton(
              icon: new Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/home');
              }),
          centerTitle: true,
          title: const Text(
            'Cadastar usuario',
            style: TextStyle(color: Colors.black),
          ),
          elevation: 0.0,
        ),
        body: SingleChildScrollView(
            child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Form(
                key: _formKey2,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: MyTextFormField(
                          labelText: "Nome",
                          hintText: 'Nome',
                          initValue: widget.users.name,
                          validator: (String value) {
                            if (value == "") {
                              return 'Nome Obrigátorio.';
                            }

                            return null;
                          },
                          onSaved: (String value) {
                            setState(() {
                              name = value;
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: MyTextFormField(
                          labelText: "Email",
                          hintText: 'Email',
                          initValue: widget.users.email,
                          validator: (String value) {
                            if (value == "") {
                              return 'CEP Obrigátorio.';
                            }

                            return null;
                          },
                          onSaved: (String value) {
                            setState(() {
                              setState(() {
                                email = value;
                              });
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: MyTextFormField(
                          labelText: "Cpf",
                          teste: maskFormatter,
                          initValue: widget.users.cpf,
                          hintText: 'Cpf',
                          validator: (String value) {
                            if (value == "") {
                              return 'CEP Obrigátorio.';
                            }

                            return null;
                          },
                          onSaved: (String value) {
                            setState(() {
                              setState(() {
                                cpf = value;
                              });
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: MyTextFormField(
                          labelText: "CEP",
                          hintText: 'CEP',
                          initValue: widget.users.cep,
                          validator: (String value) {
                            if (value == "") {
                              return 'CEP Obrigátorio.';
                            }

                            return null;
                          },
                          onSaved: (String value) {
                            setState(() {
                              cep = value;
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: MyTextFormField(
                          labelText: "Rua",
                          hintText: 'Rua',
                          initValue: widget.users.endereco,
                          validator: (String value) {
                            if (value == "") {
                              return 'Rua Obrigátorio.';
                            }

                            return null;
                          },
                          onSaved: (String value) {
                            setState(() {
                              rua = value;
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: MyTextFormField(
                          labelText: "Numero",
                          hintText: 'Numero',
                          initValue: widget.users.numero.toString(),
                          validator: (String value) {
                            if (value == "") {
                              return 'Numero Obrigátorio.';
                            }
                            return null;
                          },
                          onSaved: (String value) {
                            setState(() {
                              setState(() {
                                numero = value;
                              });
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: MyTextFormField(
                          labelText: "Bairro",
                          hintText: 'Bairro',
                          initValue: widget.users.bairro,
                          validator: (String value) {
                            if (value == "") {
                              return 'Bairro Obrigátorio.';
                            }

                            return null;
                          },
                          onSaved: (String value) {
                            setState(() {
                              bairro = value;
                            });
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.90,
                        child: MyTextFormField(
                          labelText: "Cidade",
                          hintText: 'Cidade',
                          initValue: widget.users.cidade,
                          validator: (String value) {
                            if (value == "") {
                              return 'Cidade Obrigátorio.';
                            }

                            return null;
                          },
                          onSaved: (String value) {
                            setState(() {
                              cidade = value;
                            });
                          },
                        ),
                      ),
                    ),
                    !loadding
                        ? Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: Container(
                              width: MediaQuery.of(context).size.width * 0.90,
                              child: RaisedButton(
                                color: Color(0xffE37740),
                                shape: RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                ),
                                onPressed: () async {
                                  if (_formKey2.currentState.validate()) {
                                    _formKey2.currentState.save();
                                    editUser();
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    child: Center(
                                      child: Text(
                                        'Salvar',
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            fontFamily: 'popins',
                                            fontWeight: FontWeight.normal,
                                            color: Colors.white,
                                            letterSpacing: .3),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: Container(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                  ],
                )),
          ],
        )));
  }
}
