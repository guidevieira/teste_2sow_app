// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'home_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$HomeController on _HomeControllerBase, Store {
  final _$user_detailsAtom = Atom(name: '_HomeControllerBase.user_details');

  @override
  ObservableStream<List<UserModel>> get user_details {
    _$user_detailsAtom.reportRead();
    return super.user_details;
  }

  @override
  set user_details(ObservableStream<List<UserModel>> value) {
    _$user_detailsAtom.reportWrite(value, super.user_details, () {
      super.user_details = value;
    });
  }

  final _$onDeleteAsyncAction = AsyncAction('_HomeControllerBase.onDelete');

  @override
  Future<dynamic> onDelete(String uuid) {
    return _$onDeleteAsyncAction.run(() => super.onDelete(uuid));
  }

  @override
  String toString() {
    return '''
user_details: ${user_details}
    ''';
  }
}
