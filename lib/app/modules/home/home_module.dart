import 'package:hasura_connect/hasura_connect.dart';
import 'package:twosow/app/Splashscreen/splashCheckin.dart';
import 'package:twosow/app/modules/Auth/login_page.dart';
import 'package:twosow/app/modules/home/CreateUser.dart';
import 'package:twosow/app/modules/home/EditUser.dart';
import 'package:twosow/app/modules/home/repositories/home_repository.dart';
import 'package:twosow/app/modules/home/home_controller.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:twosow/app/modules/home/home_page.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeController(i.get<HomeRepository>())),

        //Repo
        Bind((i) => HomeRepository(i.get<HasuraConnect>())),

        //Outros
        Bind((i) =>
            HasuraConnect("https://tight-rooster-82.hasura.app/v1/graphql")),
      ];

  @override
  List<Router> get routers => [
        Router(Modular.initialRoute, child: (_, args) => SplashCheckin()),
        Router('/home', child: (_, args) => HomePage()),
        Router('/login', child: (_, args) => LoginPage()),
        Router('/createUser', child: (_, args) => CreateUser()),
        Router('/editUser', child: (_, args) => EditUser(users: args.data)),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
