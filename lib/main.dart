import 'package:flutter/material.dart';
import 'package:twosow/app/app_module.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.light));

  runApp(ModularApp(module: AppModule()));
}
